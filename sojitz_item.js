/** User     :双日新新都市開発株式会社
 *  System   :販売管理システム
 *  App      :商材/メーカマスタ
 *  Function :画面制御
 *  Create   :2018.03.09 by fukui
 *  **********************************************************
 *  Update   :YYYY.MM.DD modifier 
 *  **********************************************************
 * 物件別建物アプリ GET 
 * マスタアプリ     GET
 * 顧客マスタ       GET
 */

//--------------------------------------
// 広域変数宣言
//--------------------------------------

jQuery.noConflict();
(function($) {
    "use strict";

	//------------------------------------------------------
    // 定数宣言
	//------------------------------------------------------
var dbg_flg = 1;
//var dbg_flg = 0;
	const EVENT_INDEX_SHOW = ['app.record.index.show'];
	const EVENT_DETAIL_SHOW = ['app.record.detail.show'];
	
	const EVENT_INDEX_EDIT_SHOW = ['app.record.index.edit.show'];
	const EVENT_CREATE_EDIT_SHOW = ['app.record.create.show','app.record.edit.show'];
	
	const EVENT_INDEX_EDIT_SUBMIT = ['app.record.index.edit.submit'];
	const EVENT_CREATE_EDIT_SUBMIT = ['app.record.create.submit','app.record.edit.submit'];

	const EVENT_INDEX_EDIT_SUCCESS = ['app.record.index.edit.submit.success'];
	const EVENT_CREATE_EDIT_SUCCESS = ['app.record.create.submit.success','app.record.edit.submit.success'];

	const EVENT_INDEX_DELETE_SUBMIT = ['app.record.index.delete.submit'];
	const EVENT_DETAIL_DELETE_SUBMIT = ['app.record.detail.delete.submit'];
	
	const EVENT_PROCESS_PROCEED = ['app.record.detail.process.proceed'];
	
//    const EVENT_CHANGE_CHK_EDIT = [
//		'app.record.create.change.chk_edit','app.record.edit.change.chk_edit'
//    ];

	//------------------------------------------------------
	// 外部変数宣言
	//------------------------------------------------------

	/* function  : 初期値設定 サブテーブル
	 * @event    : 画面情報
	 */
	function init_stbl(event){
        var record = event.record;
        var tbl_maker = record['tbl_maker']['value'];
        var tbl_len = tbl_maker.length;
        
        // 初期値あり
        if(tbl_len === 1){
            // 終了
            if(tbl_maker[0]['value']['txt_maker_cd']['value']){
                return ;
            }
        }
        
        // サブテーブル初期化
        tbl_maker.length = 0;
        
        // 初期値設定
        var init_maker = [];
        var ps = 0;
        init_maker[ps] = {'v1':'0','v2':'取付費'};
        ps++;
        init_maker[ps] = {'v1':'97','v2':'セット割引'};
        ps++;
        init_maker[ps] = {'v1':'98','v2':'出精値引'};
        ps++;
        init_maker[ps] = {'v1':'99','v2':'来場特典'};

        for(var i = 0; i < init_maker.length; i++){
            var tbl_buf = {
                'txt_maker_cd' : {
                    'type' : 'SINGLE_LINE_TEXT',
                    'value' : init_maker[i]['v1']
                },
                'txt_maker_name' : {
                    'type' : 'SINGLE_LINE_TEXT',
                    'value' : init_maker[i]['v2']
                }
            };
            var tbl_param = {};
            tbl_param['value'] = tbl_buf;
            tbl_maker.push(tbl_param);
        }
	}

	//--------------------------------------
	// 詳細画面制御
	//--------------------------------------
//	kintone.events.on(EVENT_DETAIL_SHOW, function(event) {
//	});
	

	//--------------------------------------
	// 登録/編集画面制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SHOW, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
		var record = event.record;

		//--------------------------------------
		// 初期値設定/複製対応
		//--------------------------------------
		if(event.type === 'app.record.create.show'){
            init_stbl(event);
		}
		
		return event;
	});
	
	//------------------------------------------------------
	// 一覧編集回避処理
	//------------------------------------------------------
	kintone.events.on(EVENT_INDEX_EDIT_SHOW, function(event) {
		//------------------------------
		// 変数宣言
		//------------------------------
		var record = event.record;
		
		//--------------------------
		// 一覧画面編集回避対応
		//--------------------------
		for(var str in record){
			if(event.record[str]){
				event.record[str]['disabled'] = true;
			}
		}
		
		return event;
	});

})(jQuery);
